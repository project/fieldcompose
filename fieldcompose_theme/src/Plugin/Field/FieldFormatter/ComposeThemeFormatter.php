<?php

namespace Drupal\fieldcompose_theme\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Symfony\Component\Yaml\Yaml;

/**
 * Plugin implementation of the 'compose_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "compose_theme_formatter",
 *   module = "compose",
 *   label = @Translation("Compose(themable)"),
 *   field_types = {
 *     "compose"
 *   }
 * )
 */
class ComposeThemeFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {
    $elements = [];
    try {
      $fieldsAnnotation = Yaml::parse($this->fieldDefinition->getSetting('fields') ?: '') ?? [];
    } catch (\Exception $e) {
      \Drupal::messenger()
        ->addWarning('Could not parse fieldcompose yaml content. Probably misformatted. See logs for more info.');
      \Drupal\Component\Utility\DeprecationHelper::backwardsCompatibleCall(\Drupal::VERSION, '10.1.0', fn() => \Drupal\Core\Utility\Error::logException(\Drupal::logger('fieldcompose'), $e), fn() => watchdog_exception('fieldcompose', $e));
      return $elements;
    }

    $elements = [
      '#attributes' => ['class' => ['fieldcompose__wrapper']],
    ];

    $entity = $items->getEntity();
    $themeElDefaults = [
      '#view_mode' => $this->viewMode,
      '#language' => $items->getLangcode(),
      '#field_name' => $this->fieldDefinition->getName(),
      '#entity_type' => $entity->getEntityTypeId(),
      '#bundle' => $entity->bundle(),
    ];

    foreach ($items as $delta => $item) {
      $itemValues = json_decode($item->getValue()['value'] ?? json_encode([]), JSON_OBJECT_AS_ARRAY);

      $elements[$delta] = [
          '#theme' => 'fieldcompose_items',
          '#items' => [],
        ] + $themeElDefaults;

      unset($itemValues['_weight']);
      foreach ($itemValues as $itemValueId => $itemValueVal) {
        $elements[$delta]['#items'][$itemValueId] = [
          'id' => $itemValueId,
          'title' => $this->getFieldTitleFromDefinition($itemValueId, $fieldsAnnotation),
          'value' => $itemValueVal,
        ];
      }
    }

    return $elements;
  }

  private function getFieldTitleFromDefinition($fieldId, $fieldsAnnotation): ?string {
    foreach ($fieldsAnnotation as $fieldAnnotation) {
      if ($fieldId === $fieldAnnotation['id']) {
        return $fieldAnnotation['_title'] ?? NULL;
      }
    }

    return NULL;
  }

}
