<?php

namespace Drupal\fieldcompose\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;

/**
 * Plugin implementation of the 'compose_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "compose_formatter",
 *   module = "compose",
 *   label = @Translation("Compose(data table)"),
 *   field_types = {
 *     "compose"
 *   }
 * )
 */
class ComposeFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode): array {

    $values = [];
    foreach ($items as $item) {
      $values[] = json_decode($item->getValue()['value'] ?? json_encode([]), JSON_OBJECT_AS_ARRAY);
    }

    return is_array($values) && !empty($values) ? [
      '#type' => 'table',
      '#header' => array_keys($values[0] ?? []),
      '#rows' => array_map(function ($val) {
        return array_values($val ?? []);
      }, $values),
    ] : [];
  }

}
