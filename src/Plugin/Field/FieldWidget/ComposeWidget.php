<?php

namespace Drupal\fieldcompose\Plugin\Field\FieldWidget;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Symfony\Component\Yaml\Yaml;

/**
 * Plugin implementation of the 'compose_default' widget.
 *
 * @FieldWidget(
 *   id = "compose_default",
 *   label = @Translation("Compose"),
 *   weight = 0,
 *   field_types = {
 *     "compose"
 *   }
 * )
 */
class ComposeWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings(): array {
    return [] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state): array {

    try {
      $fieldsAnnotation = Yaml::parse($this->fieldDefinition->getSetting('fields') ?: '') ?? [];
    } catch (\Exception $e) {
      \Drupal::messenger()
        ->addWarning('Could not parse fieldcompose yaml content. Probably misformatted. See logs for more info.');
      \Drupal\Component\Utility\DeprecationHelper::backwardsCompatibleCall(\Drupal::VERSION, '10.1.0', fn() => \Drupal\Core\Utility\Error::logException(\Drupal::logger('fieldcompose'), $e), fn() => watchdog_exception('fieldcompose', $e));
      return $element;
    }

    $item = $items[$delta];

    $fieldIds = array_filter(array_map(function ($fieldAnnotation) {
      return $fieldAnnotation['id'] ?? NULL;
    }, $fieldsAnnotation));

    if (!empty($item->value)) {
      $values = json_decode($item->value, JSON_OBJECT_AS_ARRAY);
    }

    foreach ($fieldIds as $key => $fieldId) {
      foreach ($fieldsAnnotation[$key] as $propName => $propVal) {
        if ($propName === 'id') {
          continue;
        }

        // Do not force required state on widget config as it will prevent saving other options.
        if ($this->isDefaultValueWidget($form_state)) {
          if ($propName === '_required') {
            continue;
          }
        }
        if ($propName[0] === '_') {

          // Handle special placeholders.
          if ($propName === '_states' && is_array($propVal)) {
            $propVal = static::processStateTokens(
              $propVal,
              $this->fieldDefinition->getName(),
              $delta,
              $form['#parents']
            );
          }

          // Convert for FAPI.
          $propName[0] = '#';
          $element[$fieldId][$propName] = $propVal;
        }
      }

      if (isset($values[$fieldId])) {
        $element[$fieldId]['#default_value'] = $values[$fieldId];
      }
    }

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state): array {

    foreach ($values as &$value) {
      if (isset($value['_original_delta'])) {
        unset($value['_original_delta']);
      }
      $value = json_encode($value, JSON_HEX_APOS | JSON_FORCE_OBJECT);
    }

    return $values;
  }

  static private function processStateTokens(array $states, $fieldName, $delta, $parents): array {

    $hasParent = !empty($parents) && count($parents) === 3;

    foreach ($states as $key => $state) {
      foreach ($state as $selector => $rule) {

        $processedSelector = '';
        if (str_contains($selector, '$field_name$')) {
          $processedSelector = str_replace('$field_name$', $fieldName, $selector);
        }

        if (str_contains($selector, '$field_item_delta$')) {
          $processedSelector = str_replace('$field_item_delta$', $delta, $processedSelector);
        }

        if ($hasParent && str_contains($selector, '$parent_field_item_delta$')) {
          $processedSelector = str_replace('$parent_field_item_delta$', $parents[1], $processedSelector);
        }

        if ($hasParent && str_contains($selector, '$parent_field_name$')) {
          $processedSelector = str_replace('$parent_field_name$', $parents[0], $processedSelector);
        }

        if ($processedSelector !== $selector) {
          $states[$key][$processedSelector] = $rule;
          unset($states[$key][$selector]);
        }
      }

    }

    return $states;
  }

}
