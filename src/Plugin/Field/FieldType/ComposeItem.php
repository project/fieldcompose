<?php

namespace Drupal\fieldcompose\Plugin\Field\FieldType;

use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\DataDefinition;
use Symfony\Component\Yaml\Yaml;

/**
 * Plugin implementation of the 'compose' field type.
 *
 * @FieldType(
 *   id = "compose",
 *   label = @Translation("Compose"),
 *   description = @Translation("Stores multi-value fields as encoded val"),
 *   default_widget = "compose_default",
 *   default_formatter = "compose_formatter",
 * )
 */
class ComposeItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings(): array {
    return ['fields' => ''];
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition): array {
    return [
      'columns' => [
        'value' => [
          'type' => 'text',
          'size' => 'big',
          'not null' => FALSE,
        ],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition): array {
    $properties['value'] = DataDefinition::create('compose_field_definition')
      ->setLabel('Compose field definition')
      ->setRequired(TRUE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data): array {

    $elements['fields'] = [
      '#type' => 'textarea',
      '#title' => 'Fields',
      '#default_value' => $this->getSettings()['fields'] ?? '',
      '#description' => [
        '#type' => 'inline_template',
        '#template' => $this->getFieldsDefinitionFieldDescription(),
        '#context' => [
          'href' => 'https://api.drupal.org/api/drupal/elements',
        ],
      ],
    ];

    $form['#attached']['library'][] = 'fieldcompose/yaml_editor';

    return $elements;
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty(): bool {
    try {
      $fieldsAnnotation = Yaml::parse(addslashes($this->getSetting('fields')) ?: '') ?? [];
      $fieldIds = array_filter(array_map(function ($fieldAnnotation) {
        return $fieldAnnotation['id'] ?? NULL;
      }, $fieldsAnnotation));

      $emptyState = [];
      foreach ($fieldIds as $fieldId) {
        $emptyState[$fieldId] = 0;
      }

      $value = $this->get('value')->getValue();
      return $value === NULL || $value === '' || $value === json_encode($emptyState);
    } catch (\Exception $e) {
      \Drupal\Component\Utility\DeprecationHelper::backwardsCompatibleCall(\Drupal::VERSION, '10.1.0', fn() => \Drupal\Core\Utility\Error::logException(\Drupal::logger('fieldcompose'), $e), fn() => watchdog_exception('fieldcompose', $e));
      return TRUE;
    }

  }

  /**
   * {@inheritdoc}
   */
  public function preSave() {
    parent::preSave();

    $currentValue = is_string($this->value) ? json_decode($this->value, JSON_OBJECT_AS_ARRAY) : $this->value;
    $this->value = json_encode($currentValue);
  }

  /**
   * Helper method to get item value decoded and returned as array.
   *
   * @return array|null
   *   Decoded value.
   */
  public function getValueAsArray(): ?array {
    if ($this->isEmpty()) {
      return NULL;
    }

    $values = json_decode($this->value, JSON_OBJECT_AS_ARRAY);
    unset($values['_weight']);

    return $values;
  }

  /**
   * Description for "Fields" field.
   *
   * @return string
   *   String containing field help markup.
   */
  private function getFieldsDefinitionFieldDescription(): string {
    return <<<FIELDS_DECSRIPTION
 <p>See Possible element on <a href="{{href}}" target="_blank">Form and render elements</a></p>
 <p>The only required property is <strong>id</strong> which is field ID</p>
 <p>All other properties must start with <strong>_</strong> which translates into <strong>#</strong> as per Form API</p>
 <p>e.g:</p>
 <pre>
-
  id: my_bool
  _type: checkbox
  _title: Enable something
-
  id: my_string_field
  _type: textfield
  _title: Enter something(State dependent field)
  _required: FALSE
  _states:
    visible:
      ':input[name="\$field_name\$[\$field_item_delta\$][my_bool]"]': { checked: true }
-
  id: my_select_field
  _type: select
  _title: Choose something
  _options:
    option_1: Option One
    option_2: Option Two
  _required: TRUE
  _default_value: option_1
</pre>
<p>A few placeholders are available to manage the field states(especially for unlimited cardinality)</p>
<ul>
  <li>\$field_name\$: Current field name</li>
  <li>\$field_item_delta\$: Current field item delta</li>
</ul>
<p>Bellow placeholders are of use when your field is embedded in subform e.g: it's a field of a Paragraph</p>
<ul>
  <li>\$parent_field_name\$: Parent field name</li>
  <li>\$parent_field_item_delta\$: Parent field item delta</li>
</ul>
<p>e.g:</p>
<pre>
_states:
  visible:
    ':input[name="\$parent_field_name\$[\$parent_field_item_delta\$][subform][\$field_name\$][\$field_item_delta\$][my_bool]"]': { checked : true }
</pre>
FIELDS_DECSRIPTION;
  }

}
