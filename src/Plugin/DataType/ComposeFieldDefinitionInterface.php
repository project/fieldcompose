<?php

namespace Drupal\fieldcompose\Plugin\DataType;

use Drupal\Core\TypedData\PrimitiveInterface;

/**
 * The compose_field_definition data type.
 */
interface ComposeFieldDefinitionInterface extends PrimitiveInterface {

}
