<?php

namespace Drupal\fieldcompose\Plugin\DataType;

use Drupal\Core\TypedData\Plugin\DataType\StringData;

/**
 * The compose_field data type.
 *
 * @DataType(
 *  id = "compose_field_definition",
 *  label = @Translation("Compose Field Definition")
 * )
 */
class ComposeFieldDefinition extends StringData implements ComposeFieldDefinitionInterface {

}
