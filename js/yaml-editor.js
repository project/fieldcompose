(function (ace) {
  "use strict";

  let textareaInstances = document.querySelectorAll("textarea[name='settings\[fields\]']");
  const textarea = textareaInstances[0];

  const editDiv = document.createElement("div");
  textarea.classList.add("visually-hidden");
  textarea.parentNode.insertBefore(editDiv, textarea);

  // Init ace editor.
  const editor = ace.edit(editDiv);
  editor.getSession().setValue(textarea.value);
  editor.getSession().setTabSize(2);
  editor.setOptions({
    minLines: 3,
    maxLines: 20
  });

  // Update textarea value.
  editor.getSession().on("change", function () {
    textarea.value = editor.getSession().getValue();
  });

  textarea.classList.add("ace-initialised");

})(window.ace);
